## 服务介绍

远程调用斑马打印机打印标签！

部署需要满足下面条件：

<img src="file:///C:/Users/ThinkPad/AppData/Roaming/marktext/images/2022-11-24-11-02-03-image.png" title="" alt="" width="621">

## 服务安装

双击打开 `zebra_webapi.exe` 初次会在根目录创建配置文件config.toml：

```toml
[config]
[config.App] 
HttpPort = 9022 
PrinerIpPort = '192.168.1.10:9100'

[config.Zaplog]
Director = 'runtime/log'
Level = 'debug'
```

HttpPort：服务端口

PrinerIpP：打印机地址 9100为默认端口 一般不改

再次启动即可开启成功！

### 将服务注册到windows服务

在目录cmd下运行：

```cmd
zebra_webapi install
```

### 打印接口调用

接口地址：http://[url]/print ，url为服务所在地址和配置的端口

方法：POST

body写入：

```json
{
    "name":"ts11t", "data":{"title":"哈哈哈哈","title2":"111"},
}
```

调用接口后，会在程序目录下创建默认模板：

```cmd
^XA
^LH00,00^FS    
^SEE:GB18030.DAT
^CI26
^CWJ,E:SIMSUN.TTF^FS
^FO40,40^GFA,1113,4400,44,:Z64:eJy12M9qFDEcB/BfCJiTjA9QJt48e1MoxkfxESoenKI0WfbQY19A6osIjnjosW9QsvTQo6mXjjQkJr9fOrNsaUlAh9Jup5/d/Plm8qcA//OSMU61VscYKimL6aq0PNuxzopsbZ3tsnV1VmZb2RGqwepsfZ3NtLaD0dZ1MCNraixvsILsWGM7srbGSrJVwRVbFYYiWxWGbrBEq4IrUVQFl23QdWHk2LyqCyPHNsk6m2NzXV1w+SMtrwtO1T/EUNksvCq7C6+GCYrVP5cUxUN/FIZZGMDtl18fsc+Feblr7w/zXDOrrDCvty3Gxugh0uf5ey4n37iMv7PdO5xLxtiKjSfpLl/sjTCHtj+cW49RoE3vSIX4jurUAwvi2Oh4sWNxOrHcp0KCvLPQ86tsl/pibGS7aceyGxOTfeK+LbasGNJlqxYL0WxsP3S2dBRWhqyykh4nh5ZH+3GFNgVyZ3OHKOhGPaYXQdP9bN2L1Y9ke1bGdolCgxxxCIWjxXqxSgUN+x8oLVbsEcg1dkwId6NketOLNdrbxTqyx37H9pLsEBdr0aqTfIN5P1t1crzKfRZ/pTFRLDYJ1FeXO9CXQafS18+rNCb7IZ6b2Zr004NSY27on8XqzRnZ03HXarTXW9bheBi673a7DsyDVhZtyUgZCE6Qfepma8lKtJtiP40Q3gmfbQ+LTX3GJjiSOOTXxd6OLMXt319eDNoNs53IdlP6JfAyVV1bflFsdPvDVsZ8giA8y6/Iso3L1pB9NVuP1vNty+3ETw9EsuOg4dlsAwrPQ3oHCKpvN37mp0D2LcDBMtbRspg/U9CUJY1ifbEp4cUaEC712rbNYxRtNIOcgCpGU6qwqa5x7WjUpKYF6CayX6Y4Xd+z+oxswFqLYqWLtjwXOJdAZ1O71JXFDqe2Mtfj3Ne5VKKlBmNwaGW2gJalMXJQbLqpDdoH5r78bQ8EgN26+9icuns9OlfvltawBrSsLU1rVsta2LLGtqzdLXuCea9RYVv2MC17o5Y9V8termmPqBqsJFu1py22aq/csgdv2du3nBlaziItZ5yms5OujqLtrCcbbMvZtOXM23KWbjmjt5z9m/6n8O+vv10Gvt4=:3BEC 
^FO220,160^AJN,50,50^FD物料信息^FS 
^FO50,260^AJN,30,30^FD 时间：{datetime} ^FS 
^FO50,320^AJN,30,30^FD 重量：{qty} {uint}  ^FS  
^FO50,380^AJN,30,30^FD 位置：{pos}  ^FS 
^FO50,440^AJN,30,30^FD 备注：{comment}  ^FS  
^FO10,10^GB600,600,5^FS 
^MD20
^PQ1,0,0,N  
^XZ
```



返回值：

```json
{
    "code": 0,  //0表示成功，1：表示有错误
    "data": {},
    "msg": "success"  //错误信息或者成功
}
```

### 直接传入zpl命令打印

接口地址：http://[url]/print_zpl ，url为服务所在地址和配置的端口

方法：POST

body写入：

```json
{
    "data":"^XA^FO50,50^A0N,50,50^FDHello, World!^FS^XZ"
}
```



## wincc 里面使用

wincc按钮天添加一个vbs脚本动作

![](C:\Users\ThinkPad\AppData\Roaming\marktext\images\2022-11-24-10-48-10-image.png)

写入如下脚本：

```visual-basic
'keys 是对应模板里的变量名  {title},{name}  
'values 对应的值
Dim keysT,valuesT,name
'打印模板名称
name = "test"  
 Dim url,paramsStr
 url = "http://192.168.3.121:9022/print"
 paramsStr = "{""name"":""" + name + """,""data"":{" 
 '拼接字符串
 'title是模板里的字段，后面为值 
    paramsStr = paramsStr + """" + "title" + """:""" + HMIRuntime.Tags("TEXT_02").Read + ""","
    paramsStr = paramsStr + """" + "title1" + """:""" + HMIRuntime.Tags("TEXT_02").Read + ""","
    paramsStr = paramsStr + """" + "title2" + """:""" + HMIRuntime.Tags("TEXT_02").Read + ""","
    paramsStr = paramsStr + """" + "title3" + """:""" + HMIRuntime.Tags("TEXT_02").Read + ""","
    'wincc for 无法使用
   ' For i = 0 To 1  
     '   paramsStr = paramsStr + """" + keys(i) + """:""" + values(i) + ""","
    'Next 
    paramsStr = Left(paramsStr, Len(paramsStr) - 1) + "}}"

    Dim oauth_http,HttpRequest
    Set oauth_http=CreateObject("MSXML2.XMLHTTP")

    oauth_http.Open "POST",url,False 

    oauth_http.setRequestHeader "Content-Type","application/json"

    oauth_http.Send(paramsStr)

    HttpRequest = oauth_http.responseText
     Msgbox HttpRequest
    Set oauth_http=Nothing  
```

## EBR里面使用

> ebr里使用和windows直接使用vbs一样

```visual-basic
Function HttpRequest(name,keys,values)
    Dim url,paramsStr
    url = "http://127.0.0.1:9022/print"
    paramsStr = "{""name"":""" + name + """,""data"":{"
    For i = 0 To UBound(keys)
        paramsStr = paramsStr + """" + keys(i) + """:""" + values(i) + ""","
    Next
    paramsStr = Left(paramsStr, Len(paramsStr) - 1) + "}}"

    Dim oauth_http
    Set oauth_http=CreateObject("Microsoft.XMLHttp")
    oauth_http.Open "POST",url,False 
    oauth_http.setRequestHeader "Content-Type","application/json"

    oauth_http.Send(paramsStr)
    HttpRequest = oauth_http.responseText
    Set oauth_http=nothing 
End Function


'keys 是对应模板里的变量名  {title},{name}  
'values 对应的值
Dim keys,values
keys = Array("title","name")
values = Array("张三","18")  

MsgBox(HttpRequest("test",keys,values))
```

## 常用zpl命令

数字代表点数，*203dpi = 8dots/mm*

比如：^FO330表示 203dpi的打印机：   330/8mm   

> 下面是摘录的官方文档，更多内容查看官方文档

 ^LH – Label Home
The ^LH command sets the label home position.
The default home position of a label is the upper-left corner (position 0,0 along the x and y axis). This
is the axis reference point for labels. Any area below and to the right of this point is available for
printing. The ^LH command changes this reference point. For instance, when working with
preprinted labels, use this command to move the reference point below the preprinted area.
This command affects only fields that come after it. It is recommended to use ^LH as one of the first
commands in the label format.
Format: ^LHx,y

^CW – Font Identifier
All built-in fonts are referenced using a one-character identifier. The ^CW command assigns a single
alphanumeric character to a font stored in DRAM, memory card, EPROM, or Flash.
If the assigned character is the same as that of a built-in font, the downloaded font is used in place of
the built-in font. The new font is printed on the label wherever the format calls for the built-in font. If
used in place of a built-in font, the change is in effect only until power is turned off.
If the assigned character is different, the downloaded font is used as an additional font. The
assignment remains in effect until a new command is issued or the printer is turned off.
Format: ^CWa,d:o.x

^A@ – Use Font Name to Call Font
The ^A@ command uses the complete name of a font, rather than the character designation used in
^A. Once a value for ^A@ is defined, it represents that font until a new font name is specified by ^A@.
Format: ^A@o,h,w,d:f.x
Parameter Details
o = field orientation Values:
N = normal
R = rotates 90 degrees (clockwise)
I = inverted 180 degrees
B = read from bottom up, 270 degrees
Default: N or the last ^FW value
h = character height 

^FD – Field Data
The ^FD command defines the data string for a field. The field data can be any printable character
except those used as command prefixes (^ and ~).
In RFID printers, it can also be used to specify passwords to write to tags.
Format: ^FDa
Comments The ^ and ~ characters can be printed by changing the prefix characters—see
^CD ~CD on page 130 and ^CT ~CT on page 142. The new prefix characters cannot be printed.
Characters with codes above 127, or the ^ and ~ characters, can be printed using the ^FH and ^FD
commands.
• ^CI13 must be selected to print a backslash (\).
For information on using soft hyphens, see Comments on the ^FB command on page 164.
Parameters Details
a =
• data to be printed (all
printers), or
• a password to be written
to a RFID tag (rfid
printers)
Values: any data string up to 3072 bytes
Default: none—a string of characters must be entered

^FS – Field Separator
The ^FS command denotes the end of the field definition. Alternatively, ^FS command can also be
issued as a single ASCII control code SI (Control-O, hexadecimal 0F).
Format: ^FS

^FO – Field Origin
The ^FO command sets a field origin, relative to the label home (^LH) position. ^FO sets the upper-
left corner of the field area by defining points along the x-axis and y-axis independent of the rotation.
Format: ^FOx,y,z
Comments If the value entered for the x or y parameter is too high, it could position the
field origin completely off the label.
Parameters Details
x = x-axis location
(in dots)
Values: 0 to 32000
Default: 0
y = y-axis location
(in dots)
Values: 0 to 32000
Default: 0
z = justification
The z parameter is only
supported in firmware
versions V60.14.x, V50.14.x,
or later.
Values:
0 = left justification
1 = right justification
2 = auto justification (script dependent)
Default: last accepted ^FW value or ^FW default
This command interacts with the field direction parameter of ^FP and with the rotation parameter of
^A. For output and examples, see Field Interactions on page 1369.
The auto justification option might cause unexpected results if variable fields or
bidirectional text are used with ^FO. For the best results with bidirectional text and/or
variable fields, use either the left of right justification option.

^MD – Media Darkness
The ^MD command adjusts the darkness relative to the current darkness setting.
Format: ^MDa
Comments The ~SD command value, if applicable, is added to the ^MD command.
Parameters Details
a = media darkness
level
Values: -30 to 30, depending on current value
Initial Value at Power Up: 0
If no value is entered, this command is ignored

^PQ – Print Quantity
The ^PQ command gives control over several printing operations. It controls the number of labels to
print, the number of labels printed before printer pauses, and the number of replications of each
serial number.
Format: ^PQq,p,r,o,e
If the o parameter is set to Y, the printer cuts but does not pause, and the printer does not pause
after every group count of labels has been printed. With the o parameter set to N (default), the
printer pauses after every group count of labels has been printed.
Parameters Details
q = total quantity of
labels to print
Values: 1 to 99,999,999
Default: 1
p = pause and cut
value (labels
between pauses)
Values: 1 to 99,999,999
Default: 0 (no pause)
r = replicates of
each serial
number
Values: 0 to 99,999,999 replicates
Default:
: 0 (no replicates)
o = override pause
count
Values:
N = no
Y = yes
Default: N
e = cut on error
label (RFID void
is an error
label)
Values:
N = no - if a cutter is installed, a cut will be made after a voided RIFD label
ONLY if a cut would be made after the non-voided label and this was the last
retry.
Y = yes - if a cutter is installed, a cut will be made after ANY voided RFID label.
Default: Y

^GB – Graphic Box
The ^GB command is used to draw boxes and lines as part of a label format. Boxes and lines are
used to highlight important information, divide labels into distinct areas, or to improve the
appearance of a label. The same format command is used for drawing either boxes or lines.
Format: ^GBw,h,t,c,r
For the w and h parameters, keep in mind that printers have a default of 6, 8, 12, or
24 dots/millimeter. This comes out to 153, 203, 300, or 600 dots per inch. To determine the values
for w and h, calculate the dimensions in millimeters and multiply by 6, 8, 12, or 24.
If the width and height are not specified, you get a solid box with its width and height as specified by
value t.
The roundness-index is used to determine a rounding-radius for each box. Formula:
rounding-radius = (rounding-index / 8) * (shorter side / 2)
where the shorter side is the lesser of the width and height (after adjusting for minimum and default
values).
Parameters Details
w = box width (in
dots)
Values: value of t to 32000
Default: value used for thickness (t) or 1
h = box height (in
dots)
Values: value of t to 32000
Default: value used for thickness (t) or 1
t = border thickness
(in dots)
Values: 1 to 32000
Default: 1
c = line color Values:
B = black
W = white
Default: B
r = degree of corner-
rounding
Values: 0 (no rounding) to 8 (heaviest rounding)
Default: 0

^XG – Recall Graphic
The ^XG command is used to recall one or more graphic images for printing. This command is used
in a label format to merge graphics, such as company logos and piece parts, with text data to form a
complete label.
An image can be recalled and resized as many times as needed in each format. Other images and
data might be added to the format.
Format: ^XGd:o.x,mx,my
Parameters Details
d = source device of
stored image
Values: R:, E:, B:, and A:
Default: search priority (R:, E:, B:, and A:)
o = name of stored
image
Values: 1 to 8 alphanumeric characters
Default: if a name is not specified, UNKNOWN is used
x = extension l Fixed Value: .GRF
mx = magnification
factor on the x-
axis
Values: 1 to 10
Default: 1
my = magnification
factor on the y-
axis
Values: 1 to 10
Default: 1
Example: This is an example of using the ^XG command to recall the image SAMPLE.GRF from
DRAM and print it in five different sizes in five different locations on the same label:
^XA
^FO100,100^XGR:SAMPLE.GRF,1,1^FS
^FO100,200^XGR:SAMPLE.GRF,2,2^FS
^FO100,300^XGR:SAMPLE.GRF,3,3^FS
^FO100,400^XGR:SAMPLE.GRF,4,4^FS
^FO100,500^XGR:SAMPLE.GRF,5,5^FS
^XZ

^GF – Graphic Field
The ^GF command allows you to download graphic field data directly into the printer’s bitmap
storage area. This command follows the conventions for any other field, meaning a field orientation
is included. The graphic field data can be placed at any location within the bitmap space.
Format: ^GFa,b,c,d,data
Parameters Details
a = compression type Values:
A = ASCII hexadecimal (follows the format for other download
commands)
B = binary (data sent after the c parameter is strictly binary)
C = compressed binary (data sent after the c parameter is in
compressed binary format. The data is compressed on the host side
using Zebra’s compression algorithm. The data is then
decompressed and placed directly into the bitmap.)
Default: A
b = binary byte count Values: 1 to 99999
This is the total number of bytes to be transmitted for the total image or the total
number of bytes that follow parameter d. For ASCII download, the parameter
should match parameter c. Out-of-range values are set to the nearest limit.
Default: command is ignored if a value is not specified
c = graphic field
count
Values: 1 to 99999
This is the total number of bytes comprising the graphic format (width x height),
which is sent as parameter d. Count divided by bytes per row gives the number
of lines in the image. This number represents the size of the image, not
necessarily the size of the data stream (see d).
Default: command is ignored if a value is not specified
d = bytes per row Values: 1 to 99999
This is the number of bytes in the downloaded data that comprise one row of the
image.
Default: command is ignored if a value is not specified
data = data Values:
ASCII hexadecimal data: 00 to FF
A string of ASCII hexadecimal numbers, two digits per image byte. CR and LF
can be inserted as needed for readability. The number of two-digit number pairs
must match the above count. Any numbers sent after count is satisfied are
ignored. A comma in the data pads the current line with 00 (white space),
minimizing the data sent. ~DN or any caret or tilde character prematurely aborts
the download.
Binary data: Strictly binary data is sent from the host. All control prefixes are
ignored until the total number of bytes needed for the graphic format is sent.
Example: This example downloads 8,000 total bytes of data and places the graphic data at location
100,100 of the bitmap. The data sent to the printer is in ASCII form.