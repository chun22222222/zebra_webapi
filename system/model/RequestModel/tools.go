/*
 * @Date: 2022-04-12 09:11:39
 * @LastEditors: 春贰
 * @gitee: https://gitee.com/chun22222222
 * @github: https://github.com/chun222
 * @Desc:
 * @LastEditTime: 2024-04-01 16:22:40
 * @FilePath: \zebra_webapi\system\model\RequestModel\tools.go
 */
package RequestModel

type PrintServer struct {
	Name string                 `json:"name" binding:"required"`
	Data map[string]interface{} `json:"data"  `
}

type PrintData struct {
	Data string `json:"data" binding:"required"`
}
