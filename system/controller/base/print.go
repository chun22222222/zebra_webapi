/*
 * @Date: 2022-04-12 09:06:05
 * @LastEditors: 春贰
 * @gitee: https://gitee.com/chun22222222
 * @github: https://github.com/chun222
 * @Desc:
 * @LastEditTime: 2024-04-01 16:24:29
 * @FilePath: \zebra_webapi\system\controller\base\print.go
 */
package base

import (
	"zebra_webapi/system/core/response"
	"zebra_webapi/system/service/printService"

	"github.com/gin-gonic/gin"

	// "zebra_webapi/system/service/md"
	"zebra_webapi/system/model/RequestModel"
)

var printSer printService.PrintService

func Print(c *gin.Context) {
	var r RequestModel.PrintServer
	err := c.ShouldBind(&r)
	if err != nil {
		response.FailWithMessage("参数绑定错误"+err.Error(), c)
		return
	}

	err = printSer.Print(r.Name, r.Data)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	response.OkWithMessage("success", c)

}

func PrintZpl(c *gin.Context) {
	var r RequestModel.PrintData
	err := c.ShouldBind(&r)
	if err != nil {
		response.FailWithMessage("参数绑定错误"+err.Error(), c)
		return
	}

	err = printSer.PrintZpl(r.Data)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	response.OkWithMessage("success", c)

}
func PrintCmd(c *gin.Context) {
	var r RequestModel.PrintData
	err := c.ShouldBind(&r)
	if err != nil {
		response.FailWithMessage("参数绑定错误"+err.Error(), c)
		return
	}

	err = printSer.PrintCmd(r.Data)
	if err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	response.OkWithMessage("success", c)

}
