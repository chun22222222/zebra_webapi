/*
 * @Date: 2022-02-16 10:07:39
 * @LastEditors: 春贰
 * @gitee: https://gitee.com/chun22222222
 * @github: https://github.com/chun222
 * @Desc:
 * @LastEditTime: 2022-12-12 14:31:23
 * @FilePath: \zebra_webapi\system\middleware\jwt.go
 */

package middleware

import (
	"bytes"
	"io/ioutil"
	"strings"
	"zebra_webapi/system/core/log"

	"github.com/gin-gonic/gin"
)

func JwtMid() gin.HandlerFunc {
	return func(c *gin.Context) {
		body, _ := ioutil.ReadAll(c.Request.Body)
		//处理wincc传过来换行符bug
		body = []byte(strings.ReplaceAll(string(body), "\r\n", ""))
		// //把读过的字节流重新放到body
		c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(body))
		//记录请求数据
		log.Write(log.Info, string(body))

		c.Next()
	}
}
