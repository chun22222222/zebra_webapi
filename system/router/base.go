/*
 * @Date: 2022-04-12 09:17:10
 * @LastEditors: 春贰
 * @gitee: https://gitee.com/chun22222222
 * @github: https://github.com/chun222
 * @Desc:
 * @LastEditTime: 2024-04-01 16:39:11
 * @FilePath: \zebra_webapi\system\router\base.go
 */

package router

import (
	"github.com/gin-gonic/gin"
	"zebra_webapi/system/controller/base"
	"zebra_webapi/system/middleware"
)

func BaseRouter(r *gin.Engine) {
	r.Use(middleware.JwtMid()).POST("/print", base.Print)
	r.Use(middleware.JwtMid()).POST("/print_zpl", base.PrintZpl)
	r.Use(middleware.JwtMid()).POST("/print_cmd", base.PrintCmd)

	// authG := r.Group("tools")
	// //只需要登录
	// authG.Use(middleware.JwtMid())
	// {
	// 	authG.POST("/sequence", tools.Sequence)
	// 	authG.POST("/genpdf", pdf.GenPdf)
	// }

}
