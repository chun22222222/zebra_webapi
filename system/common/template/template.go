/*
 * @Date: 2022-11-08 10:38:03
 * @LastEditors: 春贰
 * @gitee: https://gitee.com/chun22222222
 * @github: https://github.com/chun222
 * @Desc:固定模板字符串
 * @LastEditTime: 2022-11-24 14:23:41
 * @FilePath: \zebra_webapi\system\common\template\template.go
 */
package template

import (
	"fmt"
	"io/ioutil"
	"strings"

	"zebra_webapi/system/core/log"
	"zebra_webapi/system/util/file"
	"zebra_webapi/system/util/regUtil"
	utilsys "zebra_webapi/system/util/sys"
)

func PrintTemlate(filename string, v map[string]interface{}) (error, string) {
	// 目标字符串
	distStr := ReadTemOrInit(filename,

		`^XA
^LH00,00^FS    
^SEE:GB18030.DAT
^CI26
^CWJ,E:SIMSUN.TTF^FS
^FO40,40^GFA,1113,4400,44,:Z64:eJy12M9qFDEcB/BfCJiTjA9QJt48e1MoxkfxESoenKI0WfbQY19A6osIjnjosW9QsvTQo6mXjjQkJr9fOrNsaUlAh9Jup5/d/Plm8qcA//OSMU61VscYKimL6aq0PNuxzopsbZ3tsnV1VmZb2RGqwepsfZ3NtLaD0dZ1MCNraixvsILsWGM7srbGSrJVwRVbFYYiWxWGbrBEq4IrUVQFl23QdWHk2LyqCyPHNsk6m2NzXV1w+SMtrwtO1T/EUNksvCq7C6+GCYrVP5cUxUN/FIZZGMDtl18fsc+Feblr7w/zXDOrrDCvty3Gxugh0uf5ey4n37iMv7PdO5xLxtiKjSfpLl/sjTCHtj+cW49RoE3vSIX4jurUAwvi2Oh4sWNxOrHcp0KCvLPQ86tsl/pibGS7aceyGxOTfeK+LbasGNJlqxYL0WxsP3S2dBRWhqyykh4nh5ZH+3GFNgVyZ3OHKOhGPaYXQdP9bN2L1Y9ke1bGdolCgxxxCIWjxXqxSgUN+x8oLVbsEcg1dkwId6NketOLNdrbxTqyx37H9pLsEBdr0aqTfIN5P1t1crzKfRZ/pTFRLDYJ1FeXO9CXQafS18+rNCb7IZ6b2Zr004NSY27on8XqzRnZ03HXarTXW9bheBi673a7DsyDVhZtyUgZCE6Qfepma8lKtJtiP40Q3gmfbQ+LTX3GJjiSOOTXxd6OLMXt319eDNoNs53IdlP6JfAyVV1bflFsdPvDVsZ8giA8y6/Iso3L1pB9NVuP1vNty+3ETw9EsuOg4dlsAwrPQ3oHCKpvN37mp0D2LcDBMtbRspg/U9CUJY1ifbEp4cUaEC712rbNYxRtNIOcgCpGU6qwqa5x7WjUpKYF6CayX6Y4Xd+z+oxswFqLYqWLtjwXOJdAZ1O71JXFDqe2Mtfj3Ne5VKKlBmNwaGW2gJalMXJQbLqpDdoH5r78bQ8EgN26+9icuns9OlfvltawBrSsLU1rVsta2LLGtqzdLXuCea9RYVv2MC17o5Y9V8termmPqBqsJFu1py22aq/csgdv2du3nBlaziItZ5yms5OujqLtrCcbbMvZtOXM23KWbjmjt5z9m/6n8O+vv10Gvt4=:3BEC 
^FO220,160^AJN,50,50^FD物料信息^FS 
^FO50,260^AJN,30,30^FD 时间：{datetime} ^FS 
^FO50,320^AJN,30,30^FD 重量：{qty} {uint}  ^FS  
^FO50,380^AJN,30,30^FD 位置：{pos}  ^FS 
^FO50,440^AJN,30,30^FD 备注：{comment}  ^FS  
^FO10,10^GB600,600,5^FS 
^MD20
^PQ1,0,0,N  
^XZ`)
	return ParseTpl(distStr, v)
}

//解析字符串
func ParseTpl(str string, Value map[string]interface{}) (error, string) {
	strParams := regUtil.GetAllMatch(str, `(?<={)(.+?)(?=})`)
	if len(strParams) > 0 {
		//获取参数值
		for _, v := range strParams {
			if v_data, ok := Value[v]; ok {
				//将参数替换为字符串表达式
				newValue := fmt.Sprintf("%v", v_data)
				str = strings.Replace(str, fmt.Sprintf("{%s}", v), newValue, -1)
			}
		}
	}
	return nil, str
}

//判断文件是否存在，不存在则写入
func ReadTemOrInit(fileName string, content string) string {
	temDir := utilsys.ExecutePath() + "\\template\\"
	fullpath := temDir + fileName + ".txt"

	if file.CheckNotExist(fullpath) {
		//创建目录
		if err := file.IsNotExistMkDir(temDir); err != nil {
			log.Write(log.Error, fileName+"创建目录失败"+err.Error())
		}
		err := ioutil.WriteFile(fullpath, []byte(content), 0777)
		if err != nil {
			log.Write(log.Error, fileName+"写入模板文件失败"+err.Error())
		}
		return content
	} else {
		//读取文件内容
		c, err := ioutil.ReadFile(fullpath)
		if err != nil {
			log.Write(log.Error, fileName+"读取模板文件失败"+err.Error())
			return content
		}
		return string(c)
	}
}
