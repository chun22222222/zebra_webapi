/*
 * @Date: 2022-02-14 14:09:57
 * @LastEditors: 春贰
 * @Desc:
 * @LastEditTime: 2022-11-23 15:03:18
 * @FilePath: \zebra_webapi\system\common\initial\config.go
 */
package initial

const ConfigToml = `[config]
[config.App] 
HttpPort = 9022 
PrinerIpPort = '192.168.1.10:9100'

[config.Zaplog]
Director = 'runtime/log'
Level = 'debug'
`
