/*
 * @Date: 2022-03-02 10:32:10
 * @LastEditors: 春贰
 * @gitee: https://gitee.com/chun22222222
 * @github: https://github.com/chun222
 * @Desc:markdown
 * @LastEditTime: 2024-04-01 16:38:10
 * @FilePath: \zebra_webapi\system\service\printService\printService.go
 */

package printService

import (
	// "fmt"

	"fmt"
	"net"
	"os"
	"os/exec"
	"strings"
	"time"
	"zebra_webapi/system/common/template"
	"zebra_webapi/system/core/config"

	"golang.org/x/text/encoding/simplifiedchinese"
	// "github.com/gin-gonic/gin"
	//"zebra_webapi/system/model/RequestModel"
	//"sort"
)

type PrintService struct {
}

func (_this *PrintService) Print(tplname string, printMap map[string]interface{}) error {

	var err error
	var printstr string

	for k, v := range printMap {
		//替换所有\r\n
		printMap[k] = strings.ReplaceAll(fmt.Sprintf("%v", v), "\r\n", "")
	}

	//固定字段
	//datetime ,time ,
	datetime := time.Now().Format("2006-01-02 15:04:05")
	date := time.Now().Format("2006-01-02")
	time := time.Now().Format("15:04:05")
	if printMap == nil {
		printMap = make(map[string]interface{})
	}

	printMap["datetime"] = datetime
	printMap["time"] = time
	printMap["date"] = date
	err, printstr = template.PrintTemlate(tplname, printMap)
	if err != nil {
		fmt.Println("tempaltefile failed, err:", err)
		return err
	}

	err = _this.PrintZpl(printstr)

	if err != nil {
		fmt.Println("print failed, err:", err)
		return err
	}

	return nil

}

//模板打印直接打印文件
func (_this *PrintService) PrintZpl(printstr string) error {

	h, err := simplifiedchinese.GBK.NewEncoder().Bytes([]byte(printstr))
	if err != nil {
		return fmt.Errorf("转码失败" + err.Error())
	}

	conn, err := net.Dial("tcp", config.Instance().Config.App.PrinerIpPort)
	if err != nil {
		fmt.Println("err :", err)
		return err
	}

	_, err = conn.Write(h) //发送打印zpl文本

	if err != nil {
		fmt.Println("recv failed, err:", err)
		return err
	}
	defer conn.Close() // 关闭连接
	return nil

}

//cmd文件copy打印 发送文件
func (_this *PrintService) PrintCmd(printstr string) error {

	//根据printerstr为内容生成文件
	file, err := os.Create("print.txt")
	if err != nil {
		fmt.Println("create file failed, err:", err)
		return err
	}
	defer file.Close()
	_, err = file.Write([]byte(printstr))
	if err != nil {
		fmt.Println("write file failed, err:", err)
		return err
	}

	//打印机名称
	printerName := config.Instance().Config.App.PrinerIpPort

	//执行cmd命令
	cmd := exec.Command("cmd", "/c", "copy print.txt \\\\"+printerName)
	err = cmd.Run()
	if err != nil {
		fmt.Println("exec failed, err:", err)
		return err

	}

	//删除文件
	err = os.Remove("print.txt")
	if err != nil {
		fmt.Println("remove file failed, err:", err)
		return err
	}

	return nil

}
