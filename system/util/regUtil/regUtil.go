/*
 * @Date: 2022-11-04 16:19:00
 * @LastEditors: 春贰
 * @gitee: https://gitee.com/chun22222222
 * @github: https://github.com/chun222
 * @Desc:
 * @LastEditTime: 2022-11-04 16:22:35
 * @FilePath: \server\system\util\regUtil\regUtil.go
 */
package regUtil

import "github.com/dlclark/regexp2"

//返回正则匹配的所有内容
func GetAllMatch(str string, regstr string) []string {
	var re []string
	reg := regexp2.MustCompile(regstr, 0)
	v, err := reg.FindStringMatch(str)

	if err != nil || v == nil {
		return re
	} else {
		re = append(re, v.String())
	}

	for {
		v, err = reg.FindNextMatch(v)
		if v == nil || err != nil {
			break
		}
		re = append(re, v.String())
	}
	return re
}
